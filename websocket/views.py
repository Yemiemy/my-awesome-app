from django.shortcuts import render
from django.http import JsonResponse, HttpResponse
from django.views.decorators.csrf import csrf_exempt
import json
from .models import ChatMessage, Connection
import boto3
# Create your views here.

@csrf_exempt
def test(request):
    return JsonResponse({"message":"hello Daud"}, status = 200)

def _parse_body(body):
    body_unicode = body.decode('utf-8')
    return json.loads(body_unicode)

@csrf_exempt
def connect(request):
    body = _parse_body(request.body)
    connection_id = body["connectionId"]

    connection = Connection.objects.create(
        connection_id=connection_id
        )
    connection.save()
    return JsonResponse({"message":"connect successfully"}, status = 200)
@csrf_exempt
def disconnect(request):
    body = _parse_body(request.body)
    connection_id = body["connectionId"]

    connection = Connection.objects.get(
        connection_id=connection_id
        )
    connection.delete()
    return JsonResponse({"message":"disconnect successfully"}, status = 200)


def _send_to_connection(connection_id, data):
    gatewayapi = boto3.client(
        "apigatewaymanagementapi",
        endpoint_url="https://6a5d8nxx37.execute-api.us-east-2.amazonaws.com/test/",
        region_name="us-east-2",
        aws_access_key_id= "AKIAW5VZT4P56434I36E",
        aws_secret_access_key="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"
    )
    return gatewayapi.post_to_connection(ConnectionId=connection_id, Data=json.dumps(data).encode('utf-8'))

@csrf_exempt
def send_message(request):
    body = _parse_body(request.body)
    print(body)
    print(body["body"]["username"])
    ChatMessage.objects.create(
        username=body["body"]["username"], 
        message=body["body"]["content"], 
        timestamp=body["body"]["timestamp"]
        )
    connections = Connection.objects.all()
    print(connections)
    data = {"messages": [body]}
    for connection_id in connections:
        print(connection_id)
        _send_to_connection(str(connection_id), data)
    return JsonResponse(data, status = 200)
    
def get_recent_messages(request):
    messages = ChatMessage.objects.all().order_by('-id')
    message_list = []
    connections = Connection.objects.all()
    
    for message in messages:
        message_dict = {"username":"", "content":"", "timestamp":""}
        message_dict["username"] = message.username
        message_dict["content"] = message.message
        message_dict["timestamp"] = message.timestamp

        message_list.append(message_dict)
    data = {"messages":message_list}
    
    for connection_id in connections:
        print(connection_id)
        _send_to_connection(str(connection_id), data)
        
    return JsonResponse(data, safe=False)




##/websocket/test/ HTTP/1.1" 200 25


#websocket URL

#  wss://6a5d8nxx37.execute-api.us-east-2.amazonaws.com/test

#connection URL
#  https://6a5d8nxx37.execute-api.us-east-2.amazonaws.com/test/@connections


###
##wss://750i86eg96.execute-api.us-east-2.amazonaws.com/test_Stage

#connection URL
##https://750i86eg96.execute-api.us-east-2.amazonaws.com/test_Stage/@connections