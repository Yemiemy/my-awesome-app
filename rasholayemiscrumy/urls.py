from django.urls import path, include
from .views import index, move_goal, add_goal, home
urlpatterns = [
    path('', index, name='index'),
    path('home/', home, name='home'),
    path('addgoal/', add_goal, name='add_goal'),
    path('movegoal/<int:goal_id>', move_goal, name='move_goal'),
    path('accounts/', include('django.contrib.auth.urls'))
]