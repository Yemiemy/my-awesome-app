from django.shortcuts import render, get_object_or_404
from django.urls import reverse
from django.http import HttpResponse
from .models import ScrumyGoals, GoalStatus
import random
from django.contrib.auth.models import User, Group
from .forms import SignupForm, CreateGoalForm

# Create your views here.
def index(request):
    my_group = Group.objects.get(name='Developer')
    if request.method == 'POST':
        form = SignupForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            user = User.objects.get(username=username)
            my_group.user_set.add(user)
            return HttpResponse('Your account has been created successfully!')
    else:
        form = SignupForm()
    return render(request, 'rasholayemiscrumy/index.html',{'form':form})

def home(request):
    current_user = request.user
    users = User.objects.all()
    weekly_goal = GoalStatus.objects.get(status_name='Weekly Goal')
    weekly_goals = weekly_goal.scrumygoals_set.all()
    daily_goal = GoalStatus.objects.get(status_name='Daily Goal')
    daily_goals = daily_goal.scrumygoals_set.all()
    done_goal = GoalStatus.objects.get(status_name='Done Goal')
    done_goals = done_goal.scrumygoals_set.all()
    verify_goal = GoalStatus.objects.get(status_name='Verify Goal')
    verify_goals = verify_goal.scrumygoals_set.all()
    dictionary = {
        'users':users,
        'user':current_user,
        'weekly_goal':weekly_goal,
        'daily_goal':daily_goal,
        'done_goal':done_goal,
        'verify_goal':verify_goal
        }
    return render(request, 'rasholayemiscrumy/home.html', dictionary)

def move_goal(request, goal_id):
    try:
        current_user = request.user
        current_goal = get_object_or_404(ScrumyGoals, goal_id=goal_id)
        goal_status = GoalStatus.objects.all()

        # Get all the users in each of the groups
        quality_group = Group.objects.get(name='Quality Assurance')
        users_in_quality = quality_group.user_set.all()
        dev_group = Group.objects.get(name='Developer')
        users_in_dev = dev_group.user_set.all()
        owner_group = Group.objects.get(name='Owner')
        users_in_owner = owner_group.user_set.all()
        admin_group = Group.objects.get(name='Admin')
        users_in_admin = admin_group.user_set.all()
        # Developer Permissions
        if current_user in users_in_dev:
            if current_goal.user == current_user:
                print('Developer baba')
                goal_status = GoalStatus.objects.all()[:3]
                if request.method == 'POST':
                    status = request.POST['form']
                    status = GoalStatus.objects.get(id=status)
                    current_goal.goal_status = status
                    current_goal.save()
            else:
                print('Dev baba')
                return HttpResponse('You are not authorised to move this goal.')
        # Quality assurance Permmisions
        elif current_user in users_in_quality:
            print('Quality baba')
            if current_goal.user == current_user:
                goal_status = GoalStatus.objects.all()[1::]
                if request.method == 'POST':
                    status = request.POST['form']
                    status = GoalStatus.objects.get(id=status)
                    current_goal.goal_status = status
                    current_goal.save()
            else:
                goal_status = GoalStatus.objects.all()[2::]
                if request.method == 'POST':
                    status = request.POST['form']
                    status = GoalStatus.objects.get(id=status)
                    current_goal.goal_status = status
                    current_goal.save()
        # Admin Permissions
        elif current_user in users_in_admin:
            print('admin baba')
            goal_status = GoalStatus.objects.all()
            if request.method == 'POST':
                status = request.POST['form']
                status = GoalStatus.objects.get(id=status)
                current_goal.goal_status = status
                current_goal.save()
        #Owner Permissions
        elif current_user in users_in_owner:
            if current_goal.user == current_user:
                print('Owner baba')
                goal_status = GoalStatus.objects.all()
                if request.method == 'POST':
                    status = request.POST['form']
                    status = GoalStatus.objects.get(id=status)
                    current_goal.goal_status = status
                    current_goal.save()
            else:
                return HttpResponse('You are not authorised to move this goal.')
    except:
        return render(request, 'exception.html', {'error': 'A record with that goal id does not exist'})
    return render(request, 'rasholayemiscrumy/move_goal.html',{'user':current_user, 'goal':current_goal, 'goal_status':goal_status})

def add_goal(request):
    current_user = request.user
    random_goal_id = random.randint(1000,9999)
    used_goal_id_list = []
    if random_goal_id not in used_goal_id_list:
        used_goal_id_list.append(random_goal_id)
    print(used_goal_id_list)

    # Get all the users in each of the groups
    quality_group = Group.objects.get(name='Quality Assurance')
    users_in_quality = quality_group.user_set.all()
    dev_group = Group.objects.get(name='Developer')
    users_in_dev = dev_group.user_set.all()
    owner_group = Group.objects.get(name='Owner')
    users_in_owner = owner_group.user_set.all()

    
    try:
        # Developer permissions
        if current_user in users_in_dev:
            #Making sure developer can only create weekly goal
            weekly_goal_status = GoalStatus.objects.get(id=1)
            if request.method == 'POST':
                form = CreateGoalForm(request.POST)
                if form.is_valid():
                    new_goal = form.save(commit=False)
                    new_goal.goal_id = random_goal_id
                    new_goal.user = current_user
                    new_goal.created_by = 'Louis'
                    new_goal.moved_by='Louis'
                    new_goal.owner = 'Louis'
                    new_goal.goal_status = weekly_goal_status
                    new_goal.save()
                    
                    return HttpResponse('You have successfully added a goal. <a href={}>View Task Board</a>'.format(reverse('home')))
            else:
                form = CreateGoalForm()
        # Quality Assurance(QA) permissions
        elif current_user in users_in_quality:
            #Making sure QA can only create weekly goal for himself alone
            weekly_goal_status = GoalStatus.objects.get(id=1)
            if request.method == 'POST':
                form = CreateGoalForm(request.POST)
                if form.is_valid():
                    new_goal = form.save(commit=False)
                    new_goal.goal_id = random_goal_id
                    new_goal.user = current_user
                    new_goal.created_by = 'Louis'
                    new_goal.moved_by='Louis'
                    new_goal.owner = 'Louis'
                    new_goal.goal_status = weekly_goal_status
                    new_goal.save()
                    
                    return HttpResponse('You have successfully added a goal. <a href={}>View Task Board</a>'.format(reverse('home')))
            else:
                form = CreateGoalForm() 
        # Owner permissions
        elif current_user in users_in_owner:
            #Making sure Owner can only create weekly goal for himself alone
            weekly_goal_status = GoalStatus.objects.get(id=1)
            if request.method == 'POST':
                form = CreateGoalForm(request.POST)
                if form.is_valid():
                    new_goal = form.save(commit=False)
                    new_goal.goal_id = random_goal_id
                    new_goal.user = current_user
                    new_goal.created_by = 'Louis'
                    new_goal.moved_by='Louis'
                    new_goal.owner = 'Louis'
                    new_goal.goal_status = weekly_goal_status
                    new_goal.save()
                    
                    return HttpResponse('You have successfully added a goal. <a href={}>View Task Board</a>'.format(reverse('home')))
            else:
                form = CreateGoalForm()
        else:
            return HttpResponse('You are not authorised to add a goal.')
    except:
        return render(request, 'exception.html', {'error': 'An error has occurred. Please Try again'})
    return render(request, 'rasholayemiscrumy/add_goal.html',{'form':form, 'user':current_user})







    # dic = ({
    #     'goal_name': 'Learn Django',
    #     'goal_id': 1,
    #     'user': User.objects.get(username='louis')
    # })